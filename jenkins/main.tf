terraform {
  backend "remote" {
    organization = "jonahlozano03"

    workspaces {
      name = "DigitalOcean_Jenkins_Droplet"
    }
  }
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.37.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

# Data source to retrieve the existing SSH key by name
data "digitalocean_ssh_key" "my_key" {
  name = "my-droplet-key" # Use the name of your SSH key as shown in DigitalOcean
}

# Create or reference a DigitalOcean project
data "digitalocean_project" "devops_project" {
  name = var.project_name
}

# Your resource definitions remain the same

resource "digitalocean_droplet" "jenkins_server" {
  image    = "ubuntu-20-04-x64"
  name     = "jenkins-server"
  region   = "ams3"
  size     = "s-2vcpu-4gb"
  ssh_keys = [data.digitalocean_ssh_key.my_key.id] # Referencing the SSH key ID
}

resource "digitalocean_firewall" "jenkins_server_access" {
  name = "jenkins-server-firewall"

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"] # All IPv4 and IPv6
  }

  # Rule for custom port 8080 from all IPv4 and IPv6
  inbound_rule {
    protocol         = "tcp"
    port_range       = "8080"
    source_addresses = ["0.0.0.0/0", "::/0"] # All IPv4 and IPv6
  }

  # Rule for custom port 50000 from all IPv4 and IPv6
  inbound_rule {
    protocol         = "tcp"
    port_range       = "50000"
    source_addresses = ["0.0.0.0/0", "::/0"] # All IPv4 and IPv6
  }

  # Allow all outbound ICMP traffic
  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # Allow all outbound TCP traffic
  outbound_rule {
    protocol              = "tcp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # Allow all outbound UDP traffic
  outbound_rule {
    protocol              = "udp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  # Assuming you have a droplet you want to attach the firewall to
  # Replace with your droplet ID or use a dynamic reference to a droplet resource.
  droplet_ids = [digitalocean_droplet.jenkins_server.id]
}

# Assign the droplet to the project
resource "digitalocean_project_resources" "project_assignment" {
  project = data.digitalocean_project.devops_project.id
  resources = [
    "do:droplet:${digitalocean_droplet.jenkins_server.id}",
    "do:firewall:${digitalocean_firewall.jenkins_server_access.id}"
  ]
}

output "ip_address" {
  value = digitalocean_droplet.jenkins_server.ipv4_address
}
