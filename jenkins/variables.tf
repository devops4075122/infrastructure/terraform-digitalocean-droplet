variable "do_token" {
  description = "DigitalOcean API token"
  type        = string
  sensitive   = true
}

variable "project_name" {
  description = "DevOps"
  type        = string
  default     = "DevOps"  # Change this to your desired project name
}